This is a small project that enables to upload crypto market data, which is used to generate charts onto PDF.

## Development

### `npm run dev`

Starts the app in dev mode.

## Built with

- React
- Typescript
- Vite
- Tailwind
- Redux toolkit