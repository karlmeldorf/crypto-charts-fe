/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'btn-bg-primary': '#0ff',
        'bg-primary-muted': '#0bcdc2',
        'secondary-color': '#c4c5cc',
      }
    },
  },
  plugins: [],
}
