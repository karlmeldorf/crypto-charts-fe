export type ChartFileStatus = "generating" | "ready" | "error";

export interface ChartFile {
    name: string;
    status: ChartFileStatus;
}
