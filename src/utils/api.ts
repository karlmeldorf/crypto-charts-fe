import axios from "axios";
import { API_BASE_PATH } from "../constants/api";

export const createChart = async (file: File) => {
    try {
        const formData = new FormData();
     
        formData.append(
            "file",
            file,
            file.name
        );

        const response = await axios.post(`${API_BASE_PATH}/chart`, formData);

        return response;
    } catch (error: unknown) {
        console.error(error, 'Failed to upload file and create a chart');
    }
}

export const getChart = async (name: string) => {
    try {
        const response = await axios.get(`${API_BASE_PATH}/charts/${name}`);

        return response;
    } catch (error: unknown) {
        console.error(error, 'Failed to fetch the chart file');
    }
}
