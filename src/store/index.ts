import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";

import files from "./modules/files";

const store = configureStore({
    reducer: {
        files
    },
});

export type MainStore = typeof store;
export type MainState = ReturnType<typeof store.getState>;
export type MainDispatch = typeof store.dispatch;

export const useMainDispatch = () => useDispatch<MainDispatch>();
export const useMainSelector: TypedUseSelectorHook<MainState> = useSelector;

export default store;
