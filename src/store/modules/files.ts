import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { MainDispatch, MainState } from "../../store";
import { ChartFile, ChartFileStatus } from "../../types";
import { createChart } from "../../utils/api";

interface FilesState {
    files: ChartFile[];
}

export const initialState: FilesState = {
    files: [],
};

export const FILES_STATE = "files";

export const filesSlice = createSlice({
    name: FILES_STATE,
    initialState,
    reducers: {
        setFiles(state, { payload }: PayloadAction<FilesState['files']>) {
            state.files = payload;
        },
        addFile(state, { payload }: PayloadAction<ChartFile>) {
            state.files.push(payload);
        },
    }
});

export const { setFiles, addFile } = filesSlice.actions;

export function updateFileStatus(name: string, status: ChartFileStatus) {
	return (dispatch: MainDispatch, getState: () => MainState) => {
		const state = getState();
		const allFiles = filesSelector(state);

        const updatedFiles = allFiles.map((file) => {
            if (file.name === name) {
                return { ...file, status: status };
            }

            return file;
        });

        dispatch(setFiles(updatedFiles));
	};
};

export function uploadFile(file: File) {
    return async (dispatch: MainDispatch) => {
        const fileNamePdfExtension = file.name.replace('.csv', '.pdf');

        dispatch(addFile({
            name: fileNamePdfExtension,
            status: "generating",
        }));

        await createChart(file);

        dispatch(updateFileStatus(fileNamePdfExtension, 'ready'));
    };
}

export const filesSelector = (state: MainState) => state[FILES_STATE].files;

export default filesSlice.reducer;
