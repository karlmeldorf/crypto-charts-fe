import React from 'react';
import FileList from './components/FileList';
import FileUploadButton from './components/FileUploadButton';
import Header from './components/Header';
import Navbar from './components/Navbar';

const App = () => {
  return (
    <div className="bg-slate-700 h-screen">
      <Navbar />
      <main className='grid grid-rows-3'>
        <div className='flex items-center w-full'>
          <Header />
        </div>
        <div className='flex w-full'>
          <div className='flex flex-col items-center w-full'>
            <FileUploadButton />
            <div className='mt-16'>
              <FileList />
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

export default App
