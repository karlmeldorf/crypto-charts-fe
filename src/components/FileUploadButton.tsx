import React, { useRef } from 'react'
import { useMainDispatch } from '../store';
import { uploadFile } from '../store/modules/files';
import Button from './Button';

const FileUploadButton = () => {
    const dispatch = useMainDispatch();

    const fileInputRef = useRef<HTMLInputElement>(null);

    const handleChange = (event: any) => {
        const selectedFile = event.target.files[0];

        if (selectedFile.type === 'text/csv') {
            dispatch(uploadFile(selectedFile));
        }
    }

    const handleClick = () => {
        if (fileInputRef.current) {
            fileInputRef.current.click();
        }
      };

    return (
        <>
            <Button onClick={handleClick}>
                Upload
            </Button>
            <input 
                type='file' 
                ref={fileInputRef} 
                onChange={handleChange} 
                hidden
            />
        </>
    );
}

export default FileUploadButton;