import React from 'react';
import { API_BASE_PATH } from '../constants/api';
import { ChartFileStatus } from '../types';
import PdfIcon from './PdfIcon';
import Spinner from './Spinner';

interface FileProps {
    name: string;
    status: ChartFileStatus;
}

const File = ({ name, status }: FileProps) => {
    return (
        <div className='flex flex-col justify-center items-center'>
            {status === 'generating' ? (
                <>
                    <Spinner />
                    <span className='mt-2 text-slate-50'>{name}</span>
                </>
            ) : (
                <a href={`${API_BASE_PATH}/charts/${name}`} target="_blank" rel="noopener noreferrer" className='flex flex-col justify-center items-center p-2 rounded-md hover:bg-slate-800 hover:opacity-80 hover:cursor-pointer'>
                    <PdfIcon />
                    <span className='mt-2 text-slate-50'>{name}</span>
                </a>
            )}
            <a href={`${API_BASE_PATH}/charts/${name}`} className='px-2 text-btn-bg-primary rounded-md hover:bg-slate-800 hover:opacity-80' target="_blank" rel="noopener noreferrer">
                View
            </a>
        </div>
    );
}

export default File;