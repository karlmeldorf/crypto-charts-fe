import React from 'react';
import cn from 'classnames';
import Spinner from './Spinner';

interface ButtonProps {
    loading?: boolean;
    variant?: 'primary' | 'ghost';
    onClick: () => void;
    children: React.ReactNode;
}

const Button = ({ loading = false, variant = 'primary', onClick, children }: ButtonProps) => {
    return (
        <>
            {variant === 'primary' ? (
                <button 
                    className={cn('bg-btn-bg-primary px-8 py-2 w-48 rounded-md hover:bg-bg-primary-muted', loading && 'bg-bg-primary-muted')}
                    onClick={onClick} 
                    disabled={loading}
                >
                    {loading ? <Spinner /> : children}
                </button>
            ) : (
                <button onClick={onClick} className='mt-2 px-2 text-btn-bg-primary rounded-md hover:bg-slate-800 hover:opacity-80'>
                    {loading ? <Spinner /> : children}
                </button>
            )}
        </>
    );
}

export default Button;