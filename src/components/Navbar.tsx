import React from 'react';

const Navbar = () => {
    return (
        <header className='flex-shrink-0 py-8'>
            <div className='container px-6'>
                <span className='text-slate-50 text-2xl font-bold hover:cursor-pointer'>Karl.</span>
            </div>
        </header>
    );
}

export default Navbar;