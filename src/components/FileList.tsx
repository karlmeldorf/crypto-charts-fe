import React from 'react'
import { useMainSelector } from '../store'
import { filesSelector } from '../store/modules/files'
import File from './File'
import Separator from './Separator'

const FileList = () => {
    const files = useMainSelector(filesSelector)

    return (
        <div className="flex flex-col justify-between items-center">
            <Separator>Generated PDF files</Separator>
            <div className="flex gap-4">
                {files.length === 0 && (
                    <span className="text-secondary-color text-l pt-2 text-center">
                        No charts generated yet
                    </span>
                )}
                {files.map(({ name, status }) => (
                    <File key={name} name={name}    status={status} />
                ))}
            </div>
        </div>
    )
}

export default FileList
