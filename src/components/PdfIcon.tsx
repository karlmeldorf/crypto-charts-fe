import React from 'react'
import PdfIconSvg from '../assets/pdf-icon.svg'

const PdfIcon = () => {
    return <img src={PdfIconSvg} alt="pdf icon" className="w-12 h-12" />
}

export default PdfIcon
