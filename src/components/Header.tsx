import React from 'react';

const Header = () => {
    return (
        <div className='flex flex-col justify-between items-center w-full'>
            <h1 className='text-slate-50 text-4xl font-bold text-center'>GENERATE CRYPTO CHARTS</h1>
            <span className='text-secondary-color text-l pt-2 text-center'>
                You upload market data. We generate beatiful charts.
            </span>
        </div>
    );
}

export default Header;