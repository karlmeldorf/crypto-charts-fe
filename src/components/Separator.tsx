import React from 'react'

interface SeparatorProps {
    children: React.ReactNode;
}

const Separator = ({ children }: SeparatorProps) => {
    return (
        <div className="relative flex py-5 items-center text-center">
            <div className="flex-grow border-t text-slate-50 w-40"></div>
                <span className="flex-shrink mx-4 text-slate-50">{children}</span>
            <div className="flex-grow border-t text-slate-50 w-40"></div>
        </div>
    );
}

export default Separator;